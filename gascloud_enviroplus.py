from gascloud.gascloud import GasCloudInterface as GasCloudMixin
from enviroplus.noise import Noise

from enviroplus import gas
from enviroplus.noise import Noise

import sounddevice

from datetime import datetime
import csv
import os
from os.path import basename

try:
    # only runs on the raspberry pi
    from bme280 import BME280

    # particulates
    from enviroplus.particulates import PMS5003, ReadTimeoutError

    # noise
    import ST7735


    try:
        from smbus2 import SMBus
    except ImportError:
        from smbus import SMBus


except:
    from mock_sensors import Mock_BME280 as BME280
    from mock_sensors import Mock_SMBus as SMBus


import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
handler = logging.FileHandler('./enviroplus.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)



class Enviroplus(GasCloudMixin):
    ''''''

    readings_file = None
    readings_file_path = None

    noise = None
    pms = None


    def __init__(self, settings_file=None, source_ref_file=None):
        super().__init__(settings_file, source_ref_file)

        self.readings_file = "readings.csv"
        self.pms = PMS5003()
        self.noise = Noise()

    def set_readings_file_path(self):

        self.readings_file_path = os.path.join(self.cwd, self.readings_file)

    def create_readings_file(self):
        if not self.readings_file_path:
            self.set_readings_file_path()

        if not os.path.exists(self.readings_file_path):
            with open(self.readings_file_path, "w") as file:
                writer = csv.writer(file)
                writer.writerow(["rtype", "seconds", "value1","value2","value3"])
                writer.writerow([0, datetime.utcnow().isoformat(),"","",""])

        return self.readings_file_path

    def delete_readings_file(self):

        if os.path.exists(self.readings_file_path):
            os.remove(self.readings_file_path)
            self.readings_file_path = None

    def round_dict(self, data, dp=1):
        '''round values in a dict - assumes all are numbers are round to same dp'''
        rounded = {}
        for k, v in data.items():
            try:
                rounded[k] = round(v, dp)
            except:
                rounded[k] = v

        return rounded

    def get_weather(self):
        # weather
        bus = SMBus(1)
        bme280 = BME280(i2c_dev=bus)

        temp = bme280.get_temperature()
        pressure = bme280.get_pressure()
        rh = bme280.get_humidity()

        readings = {'temp': temp, 'pressure': pressure, 'rh': rh}

        return self.round_dict(readings)

    def get_gases(self):
        '''
         {'oxidising': float Ohms,
          'reducing': float Ohms,
           nh3': float Ohms,
           'adc': float Volts
         }
        :return:
        '''
        gases = gas.read_all()

        readings = {'oxidising': gases.oxidising, 'reducing': gases.reducing, 'nh3': gases.nh3, 'adc': gases.adc}

        return self.round_dict(readings)

    def get_particulates(self):

        try:
            readings = self.pms.read()
            print(readings)
        except ReadTimeoutError:
           readings = {}

        return self.round_dict(readings)

    def get_noise(self):

        low, mid, high, amp = self.noise.get_noise_profile()
        low *= 128
        mid *= 128
        high *= 128
        amp *= 64

        readings =  {'low': low, 'mid': mid, 'high': high, 'amp': amp}
        return self.round_dict(readings)

    def write_reading(self, reading, csvwriter):
        '''called at log_interval to write current readings to database'''

        csvwriter.writerow(reading)

    def make_batch(self):
            '''take data from readings.csv and create a batch from them the clear down readings.csv'''

            #TODO: check readings file exists and has more than 1 line


            if not os.path.exists(self.settings['BATCH_DIR_PENDING']):
                os.mkdir(self.settings['BATCH_DIR_PENDING'])


            if self.settings['GADGET_ID'] > ' ':
                gadget_id = self.settings['GADGET_ID']

            # check the device key is available - this is the key for the device that is uploading,
            # not the device that is generating the data - although in this instance they are the same thing.
            device_key = self.get_devicekey()


            # create filename with date and load data from sqlite db
            when = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            last_write = self.get_last_source_ref()
            next_sequence = int(last_write['sequence'])+1
            source_ref = "%s_%0.6d_%s" % (device_key, next_sequence, datetime.utcnow().strftime("%Y%m%d%H%M%S"))



            fname = os.path.join(self.settings['BATCH_DIR_PENDING'],"%s.csv" % source_ref)

            #TODO: work without having to stop logging

            # move current readings file to pending directory and assuming method logging data will create a new readings file
            os.rename(self.readings_file_path, fname)

            #TODO: put something useful in range_written
            range_written = [0,1]
            # if no data, then nothing to do - might want to make a setting so downloads anyway
            # if not range_written[0]:
            #     print("No data to download")
            #     return

            # name of yamlfile to go with it
            yamlfile = os.path.join(self.settings['BATCH_DIR_PENDING'], "%s.yaml" % source_ref)

            # names of files we are going to upload
            filelist = [fname,yamlfile]

            # create yamlfile with details of our batch
            self.make_yaml(yamlfile, self.settings['BATCH_TYPE'], self.settings['BATCH_MODE'], device_key, source_ref, when,  filelist, gadget_id=gadget_id, range_written=range_written)

            # create zipfile but use the source_ref, it will be renamed when we have a batchid
            tempzipname = os.path.join( self.settings['BATCH_DIR_PENDING'], "%s.zip" % source_ref)

            zip_size, zip_md5 = self.make_zipfile(tempzipname, filelist)

            # make yaml file with details of batch
            payload = {
                'key': device_key,
                'source_ref': source_ref,
                'filelist': "%s,%s" % (basename(filelist[0]), basename(filelist[1])),
                'batch_type': self.settings['BATCH_TYPE'],
                'batch_mode': self.settings['BATCH_MODE'],
                'zip_size': zip_size,
                'zip_md5': zip_md5,

            }
            payload_yaml_name = os.path.join(self.settings['BATCH_DIR_PENDING'], "%s.yaml" % source_ref)
            self.make_payload_yaml(payload_yaml_name, payload)

            self.put_next_source_ref(next_sequence, source_ref, zip_size, zip_md5, datetime.utcnow().isoformat())

            # now delete readings so don't get duplicates
            # delete readings now we have the zipfile

            if self.settings['DELETE_READING_ON_ZIP']:
                logger.info("Deleting readings between %s and %s" % (range_written[0], range_written[1]))
                self.delete_readings_from_db(self.settings['DBNAME'], range_written[0], range_written[1])


            logger.info("Created batch %s" % source_ref)




    def create_table_if_not_exists(self):
        #NOT CURRENTLY USED
        sql = f'''
            CREATE TABLE IF NOT EXISTS {self.db_table} (
            rdg_no integer PRIMARY KEY AUTOINCREMENT,
            timestamp text NOT NULL,
            temp REAL,
            rh REAL
            );
            '''
        self.db.execute(sql)