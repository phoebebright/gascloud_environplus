import random

class Mock_BME280():

    def __init__(self, *args, **kwargs):
        pass

    def get_temperature(self):
        return random.uniform(10.0, 25.0)

    def get_pressure(self):
        return random.uniform(100.0, 1000.0)

    def get_humidity(self):
        return random.uniform(20.0, 100.0)

class Mock_SMBus():


    def __init__(self, *args, **kwargs):
        pass

