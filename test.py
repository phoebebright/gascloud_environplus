import os
import pytest
from log_data import Enviroplus
import copy
import csv
import time
from pid import PidFile

import threading
import daemon

PIDNAME = "/var/run/log-data.pid"


def log_data_thread():
    env = Enviroplus()

    env.readings_file = 'daemon_readings.csv'
    env.create_readings_file()
    reading = {'rtype': 10, 'seconds': 1, 'value1': 20.244}

    with open(env.readings_file_path, "a") as file:
        csvwriter = csv.DictWriter(file, fieldnames=reading.keys())
        env.write_reading(reading, csvwriter)



def log_data_daemon():

        print(f"current pid is {os.getpid()}")
        env = Enviroplus()

        env.readings_file = 'daemon_readings.csv'
        env.create_readings_file()

        pid_file = PidFile(pidname=PIDNAME)

        with daemon.DaemonContext(pidfile=pid_file):

                reading = {'rtype': 10, 'seconds': 1, 'value1': 20.244}

                with open(env.readings_file_path, "a") as file:
                    csvwriter = csv.DictWriter(file, fieldnames=reading.keys())

                    while True:
                        env.write_reading(reading, csvwriter)
                        time.sleep(1)


class TestEnvironPlus:

    @classmethod
    def setup_class(cls):
        cls.readings_file = "test_readings.csv"


    @classmethod
    def teardown_class(cls):
        print('teardown class')

    def setup_method(self, method):
        if os.path.exists("test_source_ref.csv"):
            os.remove("test_source_ref.csv")
        self.env = Enviroplus(settings_file="test_settings.yaml", source_ref_file="test_source_ref.csv")
        self.env.readings_file = self.readings_file
        self.env.create_readings_file()

    def teardown_method(self, method):
        self.env.delete_readings_file()
        os.remove(self.env.source_ref_file)

    def test_create_source_ref_file(self):

        test_file = os.path.join(self.env.cwd, "test_source_ref2.csv")
        assert not os.path.exists(test_file)
        pi = Enviroplus(source_ref_file=test_file)
        assert os.path.exists(test_file)

        os.remove(test_file)

    def test_create_delete_file(self):
        '''test that readings file can be created and deleted'''

        original_file =  copy.copy(self.env.readings_file_path)

        # set a new readings_file name and create new file
        self.env.readings_file = "testing.csv"
        self.env.set_readings_file_path()
        assert not os.path.exists(self.env.readings_file_path)

        self.env.create_readings_file()
        assert os.path.exists(self.env.readings_file_path)

        # now delete it

        file_to_delete = copy.copy(self.env.readings_file_path)
        self.env.delete_readings_file()
        assert not os.path.exists(file_to_delete)
        assert self.env.readings_file_path == None

        # now put back original file so teardown doesn't fail
        self.env.readings_file_path = original_file

    def test_weather_readings(self):

        reading = self.env.get_weather()

        assert 'temp' in reading
        assert 'rh' in reading
        assert 'pressure' in reading

        assert 10 < reading['temp'] < 25

    def test_write_reading(self):

        reading = { 'rtype': 10, 'seconds': 1, 'value1' : 20.244 }

        with open(self.env.readings_file_path, "a") as file:
            csvwriter = csv.DictWriter(file, fieldnames=reading.keys())
            self.env.write_reading(reading, csvwriter)

        with open(self.env.readings_file_path, 'r') as file:
            reader = csv.DictReader(file)
            n = 0
            for row in reader:
                print(row)
                n += 1


        # first row with timestamp + 1 readings row
        assert n == 2
        assert row['rtype'] == '10'
        assert row['seconds'] == '1'
        assert row['value1'] == '20.244'

    @pytest.mark.skip(reason="Need to get pid file writing etc.")
    def test_replace_readings_while_recording(self):
        '''check that we can seemlessly replace the readings file with an empty one'''

        # start logging
        log = threading.Thread(target=log_data_thread, name='log-data')

        # start logging and check readings file exists
        log.start()
        print('start')
        time.sleep(1)
        assert os.path.exists(os.path.join(self.env.cwd, "daemon_readings.csv"))


        time.sleep(30)
        print('stop')
        log._stop()
    #
    # def test_replace_readings_while_recording(self):
    #     '''check that we can seemlessly replace the readings file with an empty one'''
    #
    #     readings_file = os.path.join(self.env.cwd, "daemon_readings.csv")
    #     os.remove(readings_file)
    #
    #     assert not os.path.exists(readings_file)
    #
    #     # start logging
    #     log_data_daemon()
    #
    #     time.sleep(1)
    #     assert os.path.exists(readings_file)
    #
    #     with open(PIDNAME) as f:
    #         looping_program_pid = int(f.read().encode())
    #
    #     print(looping_program_pid)
    #
    #
    #     time.sleep(3)
    #     os.kill(looping_program_pid)
    #     print('stop')



