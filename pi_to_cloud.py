from gascloud_enviroplus import Enviroplus
import time
import os
from pathlib import Path

#from daemons import daemonizer


#@daemonizer.run(pidfile="/tmp/environ.pid")
def main():

    settings_file = os.path.join(Path.cwd(), "settings.yaml")

    pi = Enviroplus(settings_file)
    pi.create_readings_file()

    #TODO: handle no readings file

    # make batch from current readings.csv

    starttime = time.time()
    while True:
        seconds = time.time() - starttime

        # make batch of current data and put it in pending diretory
        pi.make_batch()

        # upload everything still in the pending directory
        pi.upload_all()

        print(f"uploading for {int(seconds)} seconds")
        # time.sleep(10)










if __name__ == '__main__':
    main()


