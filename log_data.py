from gascloud_enviroplus import Enviroplus


import time
import csv
import os
from pathlib import Path
#from daemons import daemonizer


#@daemonizer.run(pidfile="/tmp/environ.pid")
def main():

    settings_file = os.path.join(Path.cwd(), "settings.yaml")

    pi = Enviroplus(settings_file)
    pi.create_readings_file()

    with open(pi.readings_file_path, "a") as file:
        csvwriter = csv.DictWriter(file, fieldnames=['rtype', 'seconds','value1','value2','value3','value4','value5'])

        starttime = time.time()


        done = False
        while not done:
            #TODO: round to 2dp
            try:
                seconds = time.time() - starttime
                reading = pi.get_weather()

                pi.write_reading({'rtype': 10, 'seconds': seconds, 'value1': reading['temp'],}, csvwriter)
                pi.write_reading({'rtype': 11, 'seconds': seconds, 'value1': reading['rh']}, csvwriter)
                pi.write_reading({'rtype': 12, 'seconds': seconds, 'value1': reading['pressure']}, csvwriter)

                reading = pi.get_gases()
                pi.write_reading({'rtype': 21, 'seconds': seconds, 'value1': reading['oxidising']}, csvwriter)
                pi.write_reading({'rtype': 22, 'seconds': seconds, 'value1': reading['reducing']}, csvwriter)
                pi.write_reading({'rtype': 23, 'seconds': seconds, 'value1': reading['nh3']}, csvwriter)
                pi.write_reading({'rtype': 24, 'seconds': seconds, 'value1': reading['adc']}, csvwriter)

                reading = pi.get_noise()
                pi.write_reading({'rtype': 31, 'seconds': seconds, 'value1': reading['low']}, csvwriter)
                pi.write_reading({'rtype': 32, 'seconds': seconds, 'value1': reading['mid']}, csvwriter)
                pi.write_reading({'rtype': 33, 'seconds': seconds, 'value1': reading['high']}, csvwriter)
                pi.write_reading({'rtype': 34, 'seconds': seconds, 'value1': reading['amp']}, csvwriter)

                reading = pi.get_particulates()

                # return {
                #     'pm1': pm1,
                #     'pm25': pm25,
                #     'pm10': pm10,
                #     'pm1a': pm1a,
                #     'pm25a': pm25a,
                #     'pm10a': pm10a,
                #     'part05': part05,
                #     'part10': part10,
                #     'pm25': pm25,
                #     'pm5': pm5,
                #     'pm10': pm10,
                #
                # }
                pi.write_reading({'rtype': 41, 'seconds': seconds, 'value1': reading['pm1'],}, csvwriter)
                pi.write_reading({'rtype': 42, 'seconds': seconds, 'value1': reading['pm25'],}, csvwriter)
                pi.write_reading({'rtype': 43, 'seconds': seconds, 'value1': reading['pm10'],}, csvwriter)


                print(f"logging for {int(seconds)} seconds")
                time.sleep(10)

            except KeyboardInterrupt:
                done = True


if __name__ == '__main__':
    main()
